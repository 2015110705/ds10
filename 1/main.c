// 2015110705 김민규
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define COMPARE(x, y) ((x) < (y) ? (-1) : (x) == (y) ? 0 : 1)

typedef struct polyNode* polyPointer;
typedef struct polyNode{
	int coef;
	int expon;
	polyPointer link;
} polyNode;

polyPointer findLast(polyPointer node);
polyPointer padd(polyPointer a, polyPointer b);
void attach(float coeeficient, int exponent, polyPointer* ptr);
void erase(polyNode **ptr);
void printList(polyPointer head);
void insert(polyPointer* head, polyPointer node);
void inputPoly(polyPointer* head, char* fileName);
polyPointer invert(polyPointer lead);

int main()
{	
	polyPointer a = NULL, b = NULL;
	polyPointer c = NULL;

	//a, b입력
	inputPoly(&a, "a.txt");
	inputPoly(&b, "b.txt");

	//계산 
	c = padd(a, b);

	//결과 출력
	printf("%8s : ", "a");
	printList(a);
	printf("%8s : ", "b");
	printList(b);
	printf("%8s : ", "a+b=c");
	printList(c);

	return 0;
}

//노드의 마지막을 찾는 함수
polyPointer findLast(polyPointer node)
{
	polyPointer temp = node;
	
	while (temp->link != NULL) {
		temp = temp->link;
	}

	return temp;
}

polyPointer padd(polyPointer a, polyPointer b)
{
	/* return a polynomail which is the sum of a and b */
	polyPointer c, rear, temp;
	int sum;
	rear = (polyPointer)malloc(sizeof(polyNode));
	c = rear;

	while (a && b) {
		switch (COMPARE(a->expon, b->expon)) {
		case -1 :
			attach(b->coef, b->expon, &rear);
			b = b->link;
			break;
		case 0 :
			sum = a->coef + b->coef;
			if (sum) {
				attach(sum, a->expon, &rear);
			}
			a = a->link; b = b->link;
			break;
		case 1 :
			attach(a->coef, a->expon, &rear);
			a = a->link;
			break;
		default :
			break;
		}
	}

	for (; a; a = a->link) attach(a->coef, a->expon, &rear);
	for (; b; b = b->link) attach(b->coef, b->expon, &rear);

	rear->link = NULL;

	/* delete extra initial node */
	temp = c; c = c->link; free(temp);
	return c;
}

void attach(float coeeficient, int exponent, polyPointer* ptr)
{
	/*	create a new node with coef = coefficient nad expon = 
		exponent, attach it to the node pointed to by ptr.
		ptr is updated to point to this new node */
	polyPointer temp;
	temp = (polyPointer)malloc(sizeof(polyNode));
	temp->coef = coeeficient;
	temp->expon = exponent;
	(*ptr)->link = temp;
	*ptr = temp;
}

void erase(polyNode **ptr)
{
	/* erase the polynomail pointed to by ptr */
	polyPointer temp;
	while (*ptr) {
		temp = *ptr;
		*ptr = (*ptr)->link;
		free(temp);
	}
}

//리스트 출력
void printList(polyPointer head)
{
	//아무것도 없을때 에러 출력
	if (head == NULL) {
		fprintf(stderr, "list is empty\n");
		exit(0);
	}

	//맨 첫번째 체크
	//지수가 0 이면
	if (head->expon == 0) {
		printf("%d", head->coef);
	}
	else {
		if (head->coef != 1) {
			printf("%d", head->coef);
		}
		else if (head->coef == -1) {
			printf("-");
		}
		printf("x^%d", head->expon);
	}
	head = head->link;

	while (head != NULL) {
		if (head->expon == 0) {
			printf("%+d", head->coef);
		}
		else {
			if (head->coef != 1) {
				printf("%+d", head->coef);
			}
			else if (head->coef == -1) {
				printf("-");
			}
			printf("x^%d", head->expon);
		}
		head = head->link;
	}
	printf("\n");
}

void insert(polyPointer* head, polyPointer node)
{
	//creation of a node
	polyPointer temp = (polyPointer)malloc(sizeof(*temp));
	temp->coef = node->coef;
	temp->expon = node->expon;
	temp->link = NULL;

	if (*head) {
		// add to non-empty list
		if ((*head)->expon < temp->expon) {
			// as a first node
			temp->link = *head;
			*head = temp;
		}
		else {
			polyPointer search = *head;
			// as an Intermediate or a last node
			while (search->link != NULL) {
				if (search->expon < temp->expon) {
					break;
				}
				search = search->link;
			}
			temp->link = search->link;
			search->link = temp;
		}

	}
	else {
		//add to empty list
		*head = temp;
	}
}

//파일로 부터 다항식 생성하기
void inputPoly(polyPointer* head, char* fileName)
{
	FILE* pInputFile = fopen(fileName, "r");
	int expon, coef;
	char sorting[20];

	//오름차순인지 내림차순인지부터 입력
	fscanf(pInputFile, "%s", sorting);

	//오름차순, 내림차순 둘다 아니면 에러 
	if (strcmp(sorting, "a") != 0 && strcmp(sorting, "d") != 0) {
		fscanf(stderr, "file input Error\n");
		exit(0);
	}
	else {
		//나머지 경우
		while (!feof(pInputFile)) {
			polyPointer newNode = (polyPointer)malloc(sizeof(polyNode));
			
			fscanf(pInputFile, "%d %d ", &coef, &expon);
			newNode->coef = coef;
			newNode->expon = expon;
			newNode->link = NULL;

			insert(head, newNode);
		}
	}
	
	fclose(pInputFile);
}