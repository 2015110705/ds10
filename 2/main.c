// 2015110705 김민규
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define FALSE	0
#define TRUE	1

#define COMPARE(x, y) ((x) < (y) ? (-1) : (x) == (y) ? 0 : 1)

typedef struct polyNode *polyPointer;
typedef struct polyNode {
	int coef;
	int expon;
	polyPointer link;
}polyNode;

void attach(float coeeficient, int exponent, polyPointer* ptr);
polyPointer cpadd(polyPointer a, polyPointer b);
void cerase(polyPointer *ptr);
polyPointer getNode();
void retNode(polyPointer node);
void printCList(polyPointer head);
void inputPolyCL(polyPointer* head, char* fileName);
void insertFront2(polyPointer* head, polyPointer node);
void insertLast(polyPointer* head, polyPointer node);

polyPointer avail = NULL;
polyPointer lastA = NULL, lastB = NULL;

int main()
{
	polyPointer a = NULL, b = NULL;
	polyPointer c = NULL;

	//a, b입력
	inputPolyCL(&a, "a.txt");
	inputPolyCL(&b, "b.txt");

	//계산 
	c = cpadd(a, b);

	//결과 출력
	printf("%8s : ", "a");
	printCList(a);
	printf("%8s : ", "b");
	printCList(b);
	printf("%8s : ", "a+b=c");
	printCList(c);

	return 0;
}

polyPointer cpadd(polyPointer a, polyPointer b)
{
	/*	polynomials a and b are singly linked circular lists
		with a header node. Return a polynomail which is
		the sum of a and b	*/
	polyPointer startA, c, lastC;
	int sum, done = FALSE;
	startA = a;							/* record start of a */
	a = a->link;						/* skip header node for a and b*/
	b = b->link;
	c = getNode();						/* get a header node for sum */
	c->expon = -1; lastC = c;
	do {
		switch (COMPARE(a->expon, b->expon)) {
		case -1:
			attach(b->coef, b->expon, &lastC);
			b = b->link;
			break;
		case 0:
			if (startA == a) done = TRUE;
			else {
				sum = a->coef + b->coef;
				if (sum) attach(sum, a->expon, &lastC);
				a = a->link; b = b->link;
			}
			break;
		case 1:
			attach(a->coef, a->expon, &lastC);
			a = a->link;
			break;

		}
	} while (!done);
	lastC->link = c;
	return c;
}

void cerase(polyPointer *ptr)
{
	/*	erase the circular list pointed to by ptr	*/
	polyPointer temp;
	if (*ptr) {
		temp = (*ptr)->link;
		(*ptr)->link = avail;
		avail = temp;
		*ptr = NULL;
	}
}

polyPointer getNode()
{
	/* provide a node for use */
	polyPointer node;
	if (avail) {
		node = avail;
		avail = avail->link;
	}
	else {
		node = (polyPointer)malloc(sizeof(*node));
	}

	return node;
}

void retNode(polyPointer node)
{
	/*	return a node to the available list	*/
	node->link = avail;
	avail = node;
}

void attach(float coeeficient, int exponent, polyPointer* ptr)
{
	/*	create a new node with coef = coefficient nad expon =
	exponent, attach it to the node pointed to by ptr.
	ptr is updated to point to this new node */
	polyPointer temp = getNode();
	temp->coef = coeeficient;
	temp->expon = exponent;
	(*ptr)->link = temp;
	*ptr = temp;
}

void printCList(polyPointer head)
{
	polyPointer search = head;

	//아무것도 없을때 에러 출력
	if (search == NULL) {
		fprintf(stderr, "list is empty\n");
		exit(0);
	}

	search = search->link;

	//맨 첫번째 체크
	//지수가 0 이면
	if (search->expon == 0) {
		printf("%d", search->coef);
	}
	else {
		if (search->coef != 1) {
			printf("%d", search->coef);
		}
		else if (search->coef == -1) {
			printf("-");
		}
		printf("x^%d", search->expon);
	}
	search = search->link;

	while (search != head) {
		if (search->expon == 0) {
			printf("%+d", search->coef);
		}
		else {
			if (search->coef != 1) {
				printf("%+d", search->coef);
			}
			else if (search->coef == -1) {
				printf("-");
			}
			printf("x^%d", search->expon);
		}
		search = search->link;
	}
	printf("\n");
}

void inputPolyCL(polyPointer* head, char* fileName)
{
	FILE* pInputFile = fopen(fileName, "r");
	int expon, coef;
	char sorting[20];

	//오름차순인지 내림차순인지부터 입력
	fscanf(pInputFile, "%s", sorting);

	//오름차순, 내림차순 둘다 아니면 에러 
	if (strcmp(sorting, "a") != 0 && strcmp(sorting, "d") != 0) {
		fscanf(stderr, "file input Error\n");
		exit(0);
	}
	else {
		//나머지 경우
		*head = getNode();
		(*head)->expon = -1;
		(*head)->link = *head;
		if (strcmp(sorting, "a") == 0) {
			while (!feof(pInputFile)) {
				polyPointer newNode = (polyPointer)malloc(sizeof(polyNode));

				fscanf(pInputFile, "%d %d ", &coef, &expon);
				newNode->coef = coef;
				newNode->expon = expon;
				newNode->link = NULL;

				insertFront2(head, newNode);
			}
		}
		else if (strcmp(sorting, "d") == 0) {
			while (!feof(pInputFile)) {
				polyPointer newNode = (polyPointer)malloc(sizeof(polyNode));

				fscanf(pInputFile, "%d %d ", &coef, &expon);
				newNode->coef = coef;
				newNode->expon = expon;
				newNode->link = NULL;

				insertLast(head, newNode);
			}
		}
	}

	fclose(pInputFile);
}

void insertFront2(polyPointer* head, polyPointer node)
{
	polyPointer search = (*head)->link;
	polyPointer newNode = getNode();
	
	newNode->coef = node->coef;
	newNode->expon = node->expon;
	newNode->link = NULL;
	
	(*head)->link = newNode;
	newNode->link = search;
	while (search->link != *head) {
		search = search->link;
	}
}

void insertLast(polyPointer* head, polyPointer node)
{
	polyPointer search = (*head)->link;
	polyPointer newNode = getNode();

	newNode->coef = node->coef;
	newNode->expon = node->expon;
	newNode->link = NULL;

	while (search->link != *head) {
		search = search->link;
	}
	search->link = newNode;
	newNode->link = *head;
}